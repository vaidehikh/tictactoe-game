import React, { Component } from 'react';

class ChoosePlayer extends Component {
    handelForm(e){
        this.props.player(e.target.player.value);
         if( e.target.player.value === ''){
           alert('Please Choose The Player');
       }
    }
   
    render(){
        return (
            <form onSubmit={(e) => this.handelForm(e)}>
            <label>Choose the player</label>
            <br/>
            <br/>
                <label>
                    Player "X"
                    <input type="radio" name="player" value="X"/>
                </label>
                <label>
                    Player "O"
                    <input type="radio" name="player" value="O"/>
                </label>
                <br/>
                <br/>
                <div>
                <label>
                    <input type="submit" value="Start" ></input>
                    </label>
                </div>
                <br/>
            </form>
        )
    }
}
export default ChoosePlayer;